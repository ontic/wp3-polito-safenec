package it.polito.dbdmg.ontic

import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.rdd.RDD

/**
 * Created by Luigi on 23/05/2015.
 */
object Normalization {


  def minmaxNormalization(data: RDD[Vector]) = {
    val dataAsArray = data.map(_.toArray)
    val numCols = dataAsArray.first().length
    val minValues = (0 until numCols).map {
      col =>
        data.map {
          record =>
            record(col)
        }.min
    }
    val maxValues = (0 until numCols).map {
      col =>
        data.map {
          record =>
            record(col)
        }.max
    }
    val normalizedData = dataAsArray.map {
      record =>
        val normalizedRecord = Vectors.dense((0 until numCols).map {
          col =>
            val value = record(col)
            var normalizedValue = 0.0
            if (minValues(col) == maxValues(col)) {
              normalizedValue = minValues(col)
            }
            else {
              normalizedValue = (value - minValues(col)) / (maxValues(col) - minValues(col))
            }
            normalizedValue.toDouble
        }.toArray)
        normalizedRecord
    }
    normalizedData
  }

  def zscoreNormalization(data: RDD[Vector]) = {
    val dataAsArray = data.map(_.toArray)
    val numCols = dataAsArray.first().length
    val n = dataAsArray.count()
    val sums = dataAsArray.reduce(
      (a, b) => a.zip(b).map(t => t._1 + t._2))
    val sumSquares = dataAsArray.fold(
      new Array[Double](numCols)
    )(
        (a, b) => a.zip(b).map(t => t._1 + t._2 * t._2)
      )
    val stdevs = sumSquares.zip(sums).map {
      case (sumSq, sum) => math.sqrt(n * sumSq - sum * sum) / n
    }
    val means = sums.map(_ / n)

    def normalize(datum: Vector) = {
      val normalizedArray = (datum.toArray, means, stdevs).zipped.map(
        (value, mean, stdev) =>
          if (stdev <= 0) (value - mean) else (value - mean) / stdev
      )
      Vectors.dense(normalizedArray)
    }

    val normalizedData = data.map(normalize)
    (normalizedData, stdevs, means)
  }


  def zscoreNormalizationStatisticsExtraction(dataIndexed: RDD[(Long,Vector)]) = {
    val dataAsArray = dataIndexed.map{
      case(i, datum) =>
        datum
    }.map(_.toArray)


    val numCols = dataAsArray.first().length
    val n = dataAsArray.count()
    val sums = dataAsArray.reduce(
      (a, b) => a.zip(b).map(t => t._1 + t._2))
    val sumSquares = dataAsArray.fold(
      new Array[Double](numCols)
    )(
        (a, b) => a.zip(b).map(t => t._1 + t._2 * t._2)
      )
    val stdevs = sumSquares.zip(sums).map {
      case (sumSq, sum) => math.sqrt(n * sumSq - sum * sum) / n
    }
    val means = sums.map(_ / n)

    def normalize(datum: Vector, index: Long) = {
      val normalizedArray = (datum.toArray, means, stdevs).zipped.map(
        (value, mean, stdev) =>
          if (stdev <= 0) (value - mean) else (value - mean) / stdev
      )
      (index,Vectors.dense(normalizedArray))
    }

    val normalizedData  = dataIndexed.map{
      case(i,datum) =>
        val res = normalize(datum, i)
        res
    }
    normalizedData
  }


  def zscoreNormalization(data: RDD[Vector], means : Array[Double] , stdevs : Array[Double]) = {
    val dataAsArray = data.map(_.toArray)
    val numCols = dataAsArray.first().length
    val n = dataAsArray.count()
    val sums = dataAsArray.reduce(
      (a, b) => a.zip(b).map(t => t._1 + t._2))
    val sumSquares = dataAsArray.fold(
      new Array[Double](numCols)
    )(
        (a, b) => a.zip(b).map(t => t._1 + t._2 * t._2)
      )

    def normalize(datum: Vector) = {
      val normalizedArray = (datum.toArray, means, stdevs).zipped.map(
        (value, mean, stdev) =>
           if (stdev <= 0) (value - mean) else (value - mean) / stdev
      )
      Vectors.dense(normalizedArray)
    }

    val normalizedData = data.map(normalize)
    normalizedData
  }
}