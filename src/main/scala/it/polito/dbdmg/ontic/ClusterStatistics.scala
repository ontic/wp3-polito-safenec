/**
 * Created by Luigi on 08/07/2015.
 */

package it.polito.dbdmg.ontic

import org.apache.spark.SparkContext._
import org.apache.spark.mllib.linalg._
import org.apache.spark.mllib.stat.{MultivariateStatisticalSummary, Statistics}
import org.apache.spark.rdd.RDD

object ClusterStatistics {

  def clusterNumberOfPoints ( cluster : RDD[(Vector)]) : Long = {
    cluster.count()
  }

  def getVarianceNormalized ( cluster : RDD[(Vector)] ) = {
    val clusterStat: MultivariateStatisticalSummary = Statistics.colStats(cluster)
    val varianceCols : Vector  = clusterStat.variance
    varianceCols
  }

  def getNumServers (serverIP : RDD[(String)]) = {
    val maskedIP = serverIP.map{
      str =>
        val splittedArray = str.split('.')
        val masked = splittedArray(0)+"."+splittedArray(1)+"."+splittedArray(2)
        masked
    }

    val serverCounts = maskedIP.map{s => (s,1)}
    val serverCountsWithReduce = serverCounts.reduceByKey(_ + _).collect()
    val numberOfDifferentServer = serverCountsWithReduce.length
    numberOfDifferentServer
  }

  def getNumClients (clientIP : RDD[(String)]) = {
    val clientsCounts = clientIP.map{s => (s,1)}
    val clientsCountsWithReduce = clientsCounts.reduceByKey(_ + _).collect()
    val numberOfDifferentClients = clientsCountsWithReduce.length
    numberOfDifferentClients
  }


  def getMinCols ( cluster : RDD[(Vector)]) = {
    val clusterStat: MultivariateStatisticalSummary = Statistics.colStats(cluster)
    val minCols : Vector = clusterStat.min
    minCols
  }

  def getMaxCols ( cluster : RDD[(Vector)]) = {
    val clusterStat: MultivariateStatisticalSummary = Statistics.colStats(cluster)
    val maxCols : Vector = clusterStat.max
    maxCols
  }

  def getAvgCols ( cluster : RDD[(Vector)]) = {
    val clusterStat: MultivariateStatisticalSummary = Statistics.colStats(cluster)
    val avgCols : Vector = clusterStat.mean
    avgCols
  }

  def getStddevCols ( cluster : RDD[(Vector)]) = {
    val clusterStat: MultivariateStatisticalSummary = Statistics.colStats(cluster)
    val varianceCols : Vector  = clusterStat.variance
    val devstdColsArray  = varianceCols.toArray.map{
      case v => math.sqrt(v)
    }
    val devstdCVector = Vectors.dense(devstdColsArray)
    devstdCVector
  }


}

