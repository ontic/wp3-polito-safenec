package it.polito.dbdmg.ontic

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.mllib.clustering._
import org.apache.spark.mllib.linalg._
import org.apache.spark.HashPartitioner
import org.apache.spark.mllib.recommendation.Rating
import org.apache.spark.mllib.stat.Statistics
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.evaluation._
import org.apache.spark.mllib.tree._
import org.apache.spark.mllib.tree.model._
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.util.MLUtils.kFold

/**
 * Created by Luigi on 18/05/2015.
 */

object safenec {

  def main(args: Array[String]) {



    val k = args(1).toInt
    val inputfile = args(0)

    val conf = new SparkConf().setAppName("Framework  Clustering And Classification")
    val sc = new SparkContext(conf)
    val rawData = sc.textFile(inputfile)

    val dataPeerToPeer = rawData.map{
      line =>
        val buffer = line.split(' ').toBuffer
        val connectionType = buffer(100).toDouble
        (connectionType, line)
    }.filter{
      case(connType, line) =>
        connType!=1 && connType!=8192
    }.map{
      case(connType, line) =>
        line
    }
    val numberDataPeerToPeer = dataPeerToPeer.count()
    val dataForClustering = dataPeerToPeer.zipWithIndex().filter{
      case(d,index) =>
        index < (numberDataPeerToPeer/2).toLong
    }.map{
      case (d,index) =>
        d
    }
    val realTimeRawData = dataPeerToPeer.zipWithIndex().filter{
      case(d,index) =>
        index > (numberDataPeerToPeer/2).toLong
    }.map{
      case (d,index) =>
        d
    }


   // val Array(dataForClustering, realTimeRawData) = dataPeerToPeer.randomSplit(Array(0.5, 0.5))
    println("Number of data: "+dataForClustering.count()+"   dimension in bit : "+dataForClustering.count()*32*14+ "  k  : " + k)

    def preprocessData(data: RDD[String]) = {
      val processedData = data.map {
        line =>
          val buffer = line.split(' ').toBuffer

          val index = Array( 73, 29 , 2, 46, 7, 51, 8 ,52, 23, 67, 22, 66, 16 ,60 )
          val features = index.map(i => buffer(i).toDouble)

          val preorderingS : Double = buffer(81).toDouble / buffer(46).toDouble
          val pnetduplicateS : Double= buffer(82).toDouble / buffer(46).toDouble
          val hopes : Double = 255*buffer(78).toDouble - 255*buffer(77).toDouble

          val derivateFeatures :  Array[Double] = Array(preorderingS, pnetduplicateS, hopes)

          val result = Array concat(features, derivateFeatures)

          val vector = Vectors.dense(result.map(_.toDouble).toArray)
          vector
      }
      processedData
    }


    val data = preprocessData(dataForClustering)
    val realTimeData = preprocessData(realTimeRawData)

    val dataNormalization = it.polito.dbdmg.ontic.Normalization.zscoreNormalization(data)
    val normalizedData = dataNormalization._1
    val stdevsForRealTimeNormalization = dataNormalization._2
    val meansForRealTimeNormalization = dataNormalization._3

  //  (2 until 11).map {
  //    kk =>
      val kmeans = new KMeans()
      kmeans.setK(k)
      // kmeans.setSeed(12345)
      val timestampKmeansStart: Long = System.currentTimeMillis / 1000
      val modelKmeans = kmeans.run(normalizedData)
      val timestampKmeansStop: Long = System.currentTimeMillis / 1000
      println("Execution time to train kmeans :  " + (timestampKmeansStop - timestampKmeansStart))

      val clusterAndDatum = normalizedData.map {
        case datum =>
          val cluster = modelKmeans.predict(datum)
          (cluster, datum)
      }

      val clusterAndDatumPartitioned = clusterAndDatum.partitionBy(new HashPartitioner(k))



    ////////////////////////////////////////////////////////////////////////STATISTICS EXTRACTION
    val dataWithClientIp =  dataForClustering.map {

      line =>
        val buffer = line.split(' ').toBuffer

        val index = Array( 73, 29 , 2, 46, 7, 51, 8 ,52, 23, 67, 22, 66, 16 ,60 )
        val features = index.map(i => buffer(i).toDouble)

        val preorderingS : Double = buffer(81).toDouble / buffer(46).toDouble
        val pnetduplicateS : Double= buffer(82).toDouble / buffer(46).toDouble
        val hopes : Double = 255*buffer(78).toDouble - 255*buffer(77).toDouble

        val clientIP = buffer(0)
        val serverIP = buffer(44)

        val derivateFeatures :  Array[Double] = Array(preorderingS, pnetduplicateS, hopes)

        val result = Array concat(features, derivateFeatures)

        val vector = Vectors.dense(result.map(_.toDouble).toArray)

        //indexID+=1
        //(indexID, clientIP,serverIP ,vector)
        ( clientIP,serverIP ,vector)
    }
    val dataIndexedwithClientIp  = dataWithClientIp.zipWithIndex()
    val dataIndexed = dataIndexedwithClientIp.map{
      case (data, index) =>
        (index, data._3)
    }
    val clientIPIndexed = dataIndexedwithClientIp.map{
      case (data, index) =>
        (index, (data._1, data._2) )
    }
    val normalizedDataIndexed = it.polito.dbdmg.ontic.Normalization.zscoreNormalizationStatisticsExtraction(dataIndexed)
    val clusterAndDatumNormalizedIndexed = normalizedDataIndexed.map {
      case (index,datum) =>
        val cluster = modelKmeans.predict(datum)
        (index, (cluster, datum))
    }
    val dataJoined = dataIndexed.join(clusterAndDatumNormalizedIndexed)
    val dataJoinedIP = dataJoined.join(clientIPIndexed)
    val clusterAndIPAndDatumNotNormalizedAndNormalized = dataJoinedIP.map{
      case (index, d)=>
        val datumNotNormalized = d._1._1
        val clusterAndDatumNormalized = d._1._2
        val cluster = clusterAndDatumNormalized._1
        val clientIP = d._2._1
        val serverIP = d._2._2
        val datumNormalized = clusterAndDatumNormalized._2
        (cluster, clientIP, serverIP,  datumNotNormalized, datumNormalized)
    }
    val statisticsForCluster = (0 until k).map{
      case numberOfCluster =>
        val clusterNotNormalized = clusterAndIPAndDatumNotNormalizedAndNormalized.filter { case (c_, cip, sip, dnotnormal, dnormal) => c_ == numberOfCluster }.map{
          case (cl_, cip, sip,  dnotnormal, dnormal) => dnotnormal
        }.persist()

        val clusterNormalized = clusterAndIPAndDatumNotNormalizedAndNormalized.filter { case (c_, cip, sip, dnotnormal, dnormal) => c_ == numberOfCluster }.map{
          case (cl_, cip, sip,  dnotnormal, dnormal) => dnormal
        }.persist()

        val clientsIP = clusterAndIPAndDatumNotNormalizedAndNormalized.filter { case (c_, cip, sip,dnotnormal, dnormal) => c_ == numberOfCluster }.map{
          case (cl_,cip,sip,dnotnormal, dnormal) => cip
        }

        val serversIP = clusterAndIPAndDatumNotNormalizedAndNormalized.filter { case (c_, cip, sip, dnotnormal, dnormal) => c_ == numberOfCluster }.map{
          case (cl_,cip,sip,  dnotnormal, dnormal) => sip
        }
        val numberOfRecord = it.polito.dbdmg.ontic.ClusterStatistics.clusterNumberOfPoints(clusterNotNormalized)
        val minColsNotNormalized = it.polito.dbdmg.ontic.ClusterStatistics.getMinCols(clusterNotNormalized)
        val minColsNormalized = it.polito.dbdmg.ontic.ClusterStatistics.getMinCols(clusterNormalized)

        val maxColsNotNormalized = it.polito.dbdmg.ontic.ClusterStatistics.getMaxCols(clusterNotNormalized)
        val maxColsNormalized = it.polito.dbdmg.ontic.ClusterStatistics.getMaxCols(clusterNormalized)

        val avgColsNotNormalized = it.polito.dbdmg.ontic.ClusterStatistics.getAvgCols(clusterNotNormalized)
        val avgColsNormalized = it.polito.dbdmg.ontic.ClusterStatistics.getAvgCols(clusterNormalized)

        val devstdColsNotNormalized = it.polito.dbdmg.ontic.ClusterStatistics.getStddevCols(clusterNotNormalized)
        val devstdColsNormalized = it.polito.dbdmg.ontic.ClusterStatistics.getStddevCols(clusterNormalized)

        val numOfDifferentClient = it.polito.dbdmg.ontic.ClusterStatistics.getNumClients(clientsIP)
        val numOfDifferentServer = it.polito.dbdmg.ontic.ClusterStatistics.getNumServers(serversIP)
        val a_i = it.polito.dbdmg.ontic.ClusterStatistics.getVarianceNormalized(clusterNormalized)




        println("\nCluster : " + numberOfCluster+ "  ||  Number of records : " + numberOfRecord+
          "  ||  #clients : "+numOfDifferentClient + "  ||  #server : " + numOfDifferentServer)

        (0 until minColsNotNormalized.size).foreach{
          i =>
            print(
              " , "+minColsNotNormalized.toArray(i)+
                ",  "+minColsNormalized.toArray(i)+
                ",  "+ maxColsNotNormalized.toArray(i) +
                " , "+ maxColsNormalized.toArray(i) +
                " , "+ avgColsNotNormalized.toArray(i) +
                " , "+ avgColsNormalized.toArray(i) +
                " , " +devstdColsNotNormalized.toArray(i)+
                "  ,  "+devstdColsNormalized.toArray(i)+
                "  ,  " + a_i.toArray(i) + " , ")
        }


        (numberOfCluster, numberOfRecord, minColsNotNormalized, maxColsNotNormalized, avgColsNotNormalized, devstdColsNotNormalized, devstdColsNormalized)
    }
    val datumNormalized = clusterAndDatumNormalizedIndexed.map{
      case ( index, clusterAndDatum) =>
        clusterAndDatum._2
    }.persist()
    val varianceAttributeForAllDataset = it.polito.dbdmg.ontic.ClusterStatistics.getVarianceNormalized(datumNormalized)
    val avgAttributeForAllDataset = it.polito.dbdmg.ontic.ClusterStatistics.getAvgCols(datumNormalized)
    print("\nVariance : ")
    varianceAttributeForAllDataset.toArray.map{x=>
      print(x+",,,,,,,,,,")
      x}
    print("\n\nAverage : ")
    avgAttributeForAllDataset.toArray.map{x=>
      print(x+",,,,,,,,,,")
      x}

    val devstdBegin = it.polito.dbdmg.ontic.ClusterStatistics.getStddevCols(normalizedDataIndexed.map(_._2))
    val devstdNow = statisticsForCluster.map(_._7)
    val varBegin = devstdBegin.toArray.map(math.pow(_, 2))
    val varNow = devstdNow.map(_.toArray.map(math.pow(_, 2)))

    val varianceReduction = varNow.map(x => x.zip(varBegin).map{case (a, b) => (b-a)/b}).zipWithIndex

    val varSortedByVarRed = varianceReduction.map(_._1.zipWithIndex.sortBy(-_._1))
    val varSortedByVar = varNow.map(_.zipWithIndex.sortBy(_._1))





   ///////////////////////////////////////////////////////////////////// //CLASSIFICATION
    val dataForClassification = clusterAndDatumPartitioned.map {
      case(cluster, datum) =>
        LabeledPoint(cluster,datum)
    }
    dataForClassification.cache()

    def getMetrics(modelTree: DecisionTreeModel, data: RDD[LabeledPoint]):
    MulticlassMetrics = {
      val predictionsAndLabels = dataForClassification.map(example =>
        (modelTree.predict(example.features), example.label)
      )
      new MulticlassMetrics(predictionsAndLabels)
    }

    def crossValDecisionTree(data : RDD[LabeledPoint], nFolds : Int = 3, seed : Int = 77) = {

      val folds = kFold(data, nFolds, seed)

      val models = folds.map { case (train, test) => (DecisionTree.trainClassifier(train, k,  Map[Int,Int](), "gini", 4, 100), test) }

      val accuracyAndPrecisionAndRecall = models.map{
        case (model, testData) =>
          val metrics = getMetrics(model, testData)
          val accuracy = metrics.precision

          val precisionAndRecall = (0 until k).map {
          cat =>
            val precision = metrics.precision(cat)
            val recall = metrics.recall(cat)
            (precision,recall)
        }.toArray
          println("Accuracy: " + accuracy)
          precisionAndRecall.map(x=>x).foreach(println)
          (accuracy , precisionAndRecall)
      }

      accuracyAndPrecisionAndRecall
    }

    val timestampTreeStart: Long = System.currentTimeMillis / 1000
    val modelTree = DecisionTree.trainClassifier(dataForClassification, k,  Map[Int,Int](), "gini", 4, 100 )
    val timestampTreeStop: Long = System.currentTimeMillis / 1000
    println("Execution time to train the classifier : " + (timestampTreeStop - timestampTreeStart))

    val timestampCVStart: Long = System.currentTimeMillis / 1000
    val crossValidationData = crossValDecisionTree(dataForClassification)
    val timestampCVStop: Long = System.currentTimeMillis / 1000
    println("Execution time for the CROSS VALIDATION  : " + (timestampCVStop - timestampCVStart))

    val dataW : RDD[(Int, Vector)] = null

    def recursiveRTEvaluation (realTimeData : RDD[Vector],oldData : RDD[(Int,Vector)] ) : Double = {

     if(realTimeData.count() < 10000){
       return 0
     }
      val timestampTreeStart: Long = System.currentTimeMillis
      val b_i = realTimeData.take(10000)
      val otherRTData = realTimeData.zipWithIndex().filter{
        case(v, i) =>
          i>10000
      }.map{case (v,i) => v}
      val b_iRDD = sc.makeRDD(b_i)

     val b_iNormalized = it.polito.dbdmg.ontic.Normalization.zscoreNormalization(b_iRDD, meansForRealTimeNormalization, stdevsForRealTimeNormalization)


      val predictedRealTimeData = b_iNormalized.map{
        case(datum) =>
          val label = modelTree.predict(datum)
          (label.toInt, datum)
      }

      val newData = predictedRealTimeData.union(oldData)

      val sse = modelKmeans.computeCost(newData.map{case (c,d) => d})
      val timestampTreeStop: Long = System.currentTimeMillis
      println("SSE for "+newData.count()+"     : "+sse+" : time(ms) : "+(timestampTreeStop - timestampTreeStart))

      val x = recursiveRTEvaluation( otherRTData, newData)
      x
    }

    val sse = modelKmeans.computeCost(clusterAndDatumPartitioned.map{case (c,d) => d})
    println("SSE initial : "+sse)
    val x = recursiveRTEvaluation( realTimeData , clusterAndDatumPartitioned)

    println("Cluster name; num.records; 1st attr. by Var Red index; average; 2nd attr. by Var Red index; average;3rd attr. by Var Red index; average; 1st by Var index; average; 2nd by Var index; average;3rd by Var index; average")
    statisticsForCluster.foreach{x =>
      val v1byvar=varSortedByVar(x._1)(0)._2
      val v2byvar=varSortedByVar(x._1)(1)._2
      val v3byvar=varSortedByVar(x._1)(2)._2
      val v1byvarRed=varSortedByVarRed(x._1)(0)._2
      val v2byvarRed=varSortedByVarRed(x._1)(1)._2
      val v3byvarRed=varSortedByVarRed(x._1)(2)._2
      val v1byvarAvg = x._5(v1byvar)
      val v2byvarAvg = x._5(v2byvar)
      val v3byvarAvg = x._5(v3byvar)
      val v1byvarRedAvg = x._5(v1byvarRed)
      val v2byvarRedAvg = x._5(v2byvarRed)
      val v3byvarRedAvg = x._5(v3byvarRed)
      println(f"cluster ${x._1}; ${x._2}; $v1byvarRed; $v1byvarRedAvg; $v2byvarRed; $v2byvarRedAvg; $v3byvarRed; $v3byvarRedAvg; $v1byvar; $v1byvarAvg; $v2byvar; $v2byvarAvg; $v3byvar; $v3byvarAvg")}

    varianceReduction.foreach(x => println(x._1.mkString(" ")))
    println()
    varNow.foreach(x => println(x.mkString(" ")))
    println("\nRanking by Variance Reduction Ratio:")
    varSortedByVarRed.foreach(x => println(x.mkString(" ")))
    varSortedByVarRed.foreach(x => println(x.map(_._2).mkString(" ")))
    println("\nRanking by Variance:")
    varSortedByVar.foreach(x => println(x.mkString(" ")))
    varSortedByVar.foreach(x => println(x.map(_._2).mkString(" ")))



  }

}
